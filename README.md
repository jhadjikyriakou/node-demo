# Node Demo

Super-basic demo node application: webserver, unit tests, pipelines

## Clone, Run, Test

1. Clone
   - `git clone git@gitlab.com:evanchiu/node-demo.git`
1. Change directory to your newly cloned source
   - `cd node-demo`
1. Install dependencies
   - `yarn install`
1. Run the project
   - `yarn start`
1. View the application at [http://localhost:3000](http://localhost:3000)
   - Kill the server by hitting Control+C
1. Run the Unit Tests
   - `yarn test`

## Modify Code

1. Open an issue and create a branch
   - From [issues](https://gitlab.com/evanchiu/node-demo/-/issues)
   - Click **New issue**
   - Fill in the title and description asking you to add votes for your favorite color
   - Assign it to yourself
   - Click **Submit Issue**
   - Click **Create Merge Request**
1. Check out your branch
   - `git fetch`
   - `git checkout XXX-your-branch-name` (substitute your branch name here)
1. Add your votes to the `favorites` data in [src/favorites.js](https://gitlab.com/evanchiu/node-demo/-/blob/master/src/favorites.js)
   - Save the file
1. Run the project again to confirm your changes worked as expected
   - `yarn start` to run the webserver
   - View the application at [http://localhost:3000](http://localhost:3000)
   - Kill the server by hitting Control+C
1. Commit the changes in your local machine
   - `git status` - check the status of your local git repository, see that files have changed
   - `git diff` - show the changes in those files
   - `git add .` - stage all the changes within this directory
   - `git status` - check the status of your local git repository, see that changes are staged
   - `git diff --staged` - show the changes that are staged for commit
   - `git commit -m "feat(favorites): added my votes to the favorites"` - commit your changes
   - See [How I Teach Git](https://rachelcarmena.github.io/2018/12/12/how-to-teach-git.html) for a great guide on understanding the differences between the git server, your local repository, the stage, and your working directory
1. Push changes to your branch on GitLab
   - `git push`
1. Watch the pipeline fail
   - Click through to the pipeline from your [merge request](https://gitlab.com/evanchiu/node-demo/-/merge_requests)
1. Fix Broken Unit Tests
   - Update expected results in [tst/favorites.test.js](https://gitlab.com/evanchiu/node-demo/-/blob/master/tst/favorites.test.js)
   - Rerun unit tests with `yarn test`
1. Amend your commit
   - `git add .` to stage your new changes
   - `git commit --amend --no-edit` to update your commit with the fixed unit tests
   - `git push --force` to update GitLab's copy of your branch
1. Assign the merge request to a reviewer
   - Find your [merge request](https://gitlab.com/evanchiu/node-demo/-/merge_requests)
   - Click "Resolve WIP Status" to remove the WIP (Work in Progress) prefix _(this indicates to your team that you're not ready for review)_
   - Assign it to Evan Chiu, or other team [member](https://gitlab.com/evanchiu/node-demo/-/project_members) with "Maintainer" or "Owner" access
   - As a merge request reviewer, you'll want to look through the commits and changes, then start discussion as necessary and approve when you're satisfied
