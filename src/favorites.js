// A toy example where we hardcode the data
function getFavorites() {
  return {
    red: 4,
    blue: 5,
    yellow: 10,
    orange:7,
    green: 5 
  };
}

module.exports = getFavorites;
